package com.example.didemo;

import com.example.didemo.controller.ConstructorBasedController;
import com.example.didemo.controller.GetterBasedController;
import com.example.didemo.controller.MyController;
import com.example.didemo.controller.PropertyBasedController;
import com.example.didemo.model.Forecast;
import com.example.didemo.model.MyBeanPostProcessor;
import com.example.didemo.model.SpringBeanLifeCicleDemo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

import java.sql.SQLOutput;

@SpringBootApplication
@ImportResource("classpath:beanConfig.xml")
public class DiDemoApplication {

    public static void main(String[] args) {

        /* ApplicationContext context = SpringApplication.run(DiDemoApplication.class, args);
        MyController controller = (MyController) context.getBean("myController"); // crea cualquier Bean
        controller.hello();

       System.out.println(context.getBean(PropertyBasedController.class).sayHello());
       System.out.println(context.getBean(GetterBasedController.class).SayHello());
       System.out.println(context.getBean(ConstructorBasedController.class).sayHello());*/

        ApplicationContext context = SpringApplication.run(DiDemoApplication.class,args);
        //MyBeanPostProcessor myBeanPostProcessor = (MyBeanPostProcessor) context.getBean("myBeanPostProcessor");

        //System.out.println(context.getBean(Forecast.class).weather());
       // System.out.println(context.getBean(ConstructorBasedController.class).sayHello());
        System.out.println(context.getBean(FakeDataSource.class).getUser());
        System.out.println(context.getBean(FakeDataSource.class).getPassword());
        System.out.println(context.getBean(FakeDataSource.class).getUrl());
        //System.out.println(context.getBean(FakeDataSource.class).getVariableDataBase_User()+"<<<<<<<<<<<");
        System.out.println(context.getBean(FakeJMS.class).getUser()+"!!!!!!");
        System.out.println(context.getBean(FakeJMS.class).getPassword()+"!!!!!!");
        System.out.println(context.getBean(FakeJMS.class).getUrl()+"!!!!!!");


    }
}
