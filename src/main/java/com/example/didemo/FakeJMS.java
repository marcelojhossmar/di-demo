package com.example.didemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class FakeJMS {
    @Value("${jms.user}")
    private String user;
    @Value("${jms.passwd}")
    private String password;
    @Value("${jms.url}")
    private String url;


    @Autowired
    private Environment env;


    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }

    public String getVariableDataBase_User(){

        return env.getProperty("DATABASE_USER");
    }

    @PostConstruct
    public void postConstruct (){
        user = env.getProperty("ENV_USER");
        System.out.println("variable user ===> "+user );
    }
}
