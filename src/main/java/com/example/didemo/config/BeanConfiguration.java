package com.example.didemo.config;


import com.example.didemo.model.Forecast;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class BeanConfiguration {

  //  @Bean
    public Forecast forecast (){
        return new Forecast();
    }
}
