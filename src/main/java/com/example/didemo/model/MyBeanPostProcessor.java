package com.example.didemo.model;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)  {
        if(bean instanceof SpringBeanLifeCicleDemo){
            ((SpringBeanLifeCicleDemo)bean).beforeInit();

        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if(bean instanceof SpringBeanLifeCicleDemo){
            ((SpringBeanLifeCicleDemo)bean).afterInit();

        }
        return bean;
    }




}
