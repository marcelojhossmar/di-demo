package com.example.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class ConstructorGreetingServiceAplication implements GreetingService  {

    public static  final String GREETING = "HOLA DESDE  CONSTRUCOR GREETING SERVICE APLICATION";

    @Override
    public String sayGreeting() {
        return GREETING;

    }

}
