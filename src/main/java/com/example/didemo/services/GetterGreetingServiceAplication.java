package com.example.didemo.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile("es")
public class GetterGreetingServiceAplication implements GreetingService  {

    public static  final String GREETING = "HOLA DESDE GETER GREETING SERVICE APLICATION";

    @Override
    public String sayGreeting() {
        return GREETING;

    }

}
