package com.example.didemo.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile("en")
public class GreetingServiceAplication implements GreetingService  {

    public static  final String GREETING = "HELLO FROM GREETING APLICATION";

    @Override
    public String sayGreeting() {
        return GREETING;

    }

}
