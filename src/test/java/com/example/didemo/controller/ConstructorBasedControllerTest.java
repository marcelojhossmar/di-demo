package com.example.didemo.controller;

import com.example.didemo.services.GreetingService;
import com.example.didemo.services.GreetingServiceAplication;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConstructorBasedControllerTest {

     private ConstructorBasedController  constructorBasedController;
    @Before
    public void setUp() throws Exception {
        System.out.println("@Before");
        GreetingService greetingService = new GreetingServiceAplication();
        //constructorBasedController = new ConstructorBasedController(greetingService);
    }

    @After
    public void tearDown() throws Exception {

        System.out.println("@After");
    }

    @Test
    public void sayHello() {
        System.out.println("@Test say hello");
        String greeting = constructorBasedController.sayHello();
        System.out.println("PRUEBA REPOSITORIO");
        Assert.assertEquals(GreetingServiceAplication.GREETING, greeting);


    }
}