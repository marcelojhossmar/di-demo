package com.example.didemo.controller;

import com.example.didemo.services.GreetingServiceAplication;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GetterBasedControllerTest {

    private GetterBasedController getterBasedController;
    @Before
    public void setUp() throws Exception {
        System.out.println("@before");
        getterBasedController= new GetterBasedController();
        getterBasedController.setGreetingService(new GreetingServiceAplication());
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("after");
    }

    @Test
    public void sayHello() {
        String greeting = getterBasedController.SayHello();
        System.out.println(greeting);
        Assert.assertEquals(GreetingServiceAplication.GREETING, greeting);
    }
}