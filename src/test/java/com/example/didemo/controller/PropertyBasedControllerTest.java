package com.example.didemo.controller;


import com.example.didemo.services.GreetingServiceAplication;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PropertyBasedControllerTest {

    private PropertyBasedController propertyBasedController;
    @Before
    public void setUp()  {
        propertyBasedController = new PropertyBasedController();
        propertyBasedController.greetingService = new GreetingServiceAplication();


    }

    @After
    public void tearDown()  {
    }

    @Test
    public void sayHello() {

         String greeting = propertyBasedController.sayHello();
         System.out.println(greeting);
         Assert.assertEquals(GreetingServiceAplication.GREETING, greeting);
    }


}